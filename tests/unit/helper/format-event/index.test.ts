import NOSTR from '../../../../src/enum/nostr';
import mockEntity from '../../../testdata/minds/entity';
import { formatEvent, formatNotice } from '../../../../src/helper/format-event';

describe('Event Formatter Tests', () => {
  it('should return a formatted EVENT payload', () => {
    expect(formatEvent('subscription-id', mockEntity[0])).toEqual(
      JSON.stringify([NOSTR.EVENT, 'subscription-id', mockEntity[0]])
    );
  });

  it('should return a formatted NOTICE payload', () => {
    expect(formatNotice('Oops! Encountered an error!')).toEqual(
      JSON.stringify([NOSTR.NOTICE, 'Oops! Encountered an error!'])
    );
  });
});
