import NOSTR from '../enum/nostr';
import reqHandler from './req';
import eventHandler from './event';
import Subscription from '../model/subscription';

const { formatNotice } = require('../helper/format-event');

const handler = (
  ws: WebSocket,
  query: string,
  subs: Map<string, Subscription>
) => {
  try {
    const event = JSON.parse(query);
    const key = event?.[0] || '';

    const subscriptionId = event?.[1] || '';
    const filters = event?.[2] || {};

    switch (key) {
      case NOSTR.REQ:
        // If the query has changed for this subscription, update it
        const active = subs.get(subscriptionId);
        if (!active || active.query !== query) {
          const sub = new Subscription(JSON.stringify(event), 0);
          subs.set(subscriptionId, sub);
        }

        // Then, handle request
        reqHandler(ws, subscriptionId, filters, subs);
        break;
      case NOSTR.EVENT:
        eventHandler(ws, event[1]);
        break;
      case NOSTR.CLOSE:
        subs.delete(subscriptionId);
        console.log(`[CLOSED]: subscription ${subscriptionId}`);
        break;
      default:
        console.error('Unsupported message type!');
        ws.send(
          formatNotice(
            '[ERROR]: Unsupported message type! Use one of the following supported keys: "REQ", "EVENT"'
          )
        );
    }
  } catch (err) {
    console.error(
      `[NOTICE]: Failed to parse Nostr message! Message: ${
        err?.data?.message || ''
      }`
    );
    ws.send(formatNotice('[ERROR]: Failed to parse Nostr message!'));
  }
};

export default handler;
