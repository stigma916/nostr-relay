import NOSTR from '../../enum/nostr';

const formatEvent = (subscriptionId: string, event: object) =>
  JSON.stringify([NOSTR.EVENT, subscriptionId, event]);

const formatNotice = (message) => JSON.stringify([NOSTR.NOTICE, message]);

export { formatEvent, formatNotice };
