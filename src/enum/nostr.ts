const NOSTR_MESSAGE = {
  REQ: 'REQ',
  EVENT: 'EVENT',
  CLOSE: 'CLOSE',
  NOTICE: 'NOTICE'
};

export default NOSTR_MESSAGE;
