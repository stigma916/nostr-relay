FROM node:16-alpine as build

# Install build dependencies
RUN apk update && apk add python3 make gcc g++ libc-dev

COPY package.json ./
COPY dist/ ./dist/

# Install project dependencies
RUN npm i --omit=dev

FROM node:16-alpine as runtime
WORKDIR /opt/app

COPY --from=build /node_modules/ ./node_modules/
COPY --from=build /dist/ ./dist/

ENV SERVER_PORT=3000
ENV MINDS_BASE_URI=https://feat-reverse-nostr-idx-2332.minds.io

EXPOSE 3000
ENTRYPOINT [ "node", "dist/index.js" ]
